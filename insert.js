var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "Al1exandra!*",
  database: "db_musicapp"
});

//insérer plusieurs occurences
con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  var sql = "INSERT INTO users (pseudo, email, firstname, lastname, createdAt, updatedAt) VALUES ?";
  var values = [
    ['Johndu33', 'john@toto.com', 'John', 'Doe', new Date(), new Date()],
    ['PeterPan', 'clochette@star.com', 'Pierre', 'Sando', new Date(), new Date()],
    ['AmyDesAnimaux', 'apple@coco.com', 'Amy', 'Soufret', new Date(), new Date()],
    ['HannahMontana', 'hanam@tota.com', 'Anna', 'Licra', new Date(), new Date()]
  ];
  con.query(sql, [values], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });
});
