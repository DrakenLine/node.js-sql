var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "Al1exandra!*",
  database: "db_musicapp"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  var sql = "CREATE TABLE IF NOT EXISTS users (pseudo VARCHAR(255),email VARCHAR(255), firstname VARCHAR(30), lastname VARCHAR(30), createdAt DATE, updatedAt DATE)";
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Table users created");
  });

  var sql = "CREATE TABLE IF NOT EXISTS musics (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), artist VARCHAR(30), creationDate DATE)";
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Table musics created");
  });
});